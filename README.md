### ABOUT ###

This is simple phonebook app.

### Prerequiste ###
* Composer installed
* PHP >= 7.1.3
* MYSQL
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension

### How do I set up? ###

* Create New MySQL Database . Example phonebookDB
* Copy repo to local directory 
* Create .env file (copy from the .env-example and adjust according to your local )
* Change DB_PORT,DB_DATABASE, DB_USERNAME,DB_PASSWORD in .env file
* Run composer install 
* Run php artisan key:generate
* Run php artisan migrate
* Run php artisan db:seed ( to load sample data) 
* Run php artisan serve
* Go to browser http://127.0.0.1:8000

