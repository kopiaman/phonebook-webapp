<?php

use App\Contact;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {

        foreach (range(1, 100) as $index) {

            Contact::create([

                'first_name' => $faker->firstName,
                'last_name'  => $faker->lastName,
                'phone_no'   => $faker->phoneNumber,
                'type'       => $faker->randomElement(['home', 'work', 'cellular', 'others']),
                'group'      => $faker->randomElement(['family', 'friends', 'officemate', 'others']),

            ]);

        }; //foreach end

    }
}
