<?php

use App\Lookup;
use Illuminate\Database\Seeder;

class LookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Lookup::truncate();

        Lookup::insert([

            [
                'category' => 'contact_type',
                'name'     => 'Work',
                'value'    => 'work',
            ],
            [
                'category' => 'contact_type',
                'name'     => 'Mobile',
                'value'    => 'mobile',
            ],
            [
                'category' => 'contact_type',
                'name'     => 'Cellular',
                'value'    => 'cellular',
            ],
            [
                'category' => 'contact_type',
                'name'     => 'Others',
                'value'    => 'others',
            ],
            [
                'category' => 'contact_group',
                'name'     => 'Friends',
                'value'    => 'friends',
            ],
            [
                'category' => 'contact_group',
                'name'     => 'Family',
                'value'    => 'family',
            ],
            [
                'category' => 'contact_group',
                'name'     => 'Office Mate',
                'value'    => 'officemate',
            ],
            [
                'category' => 'contact_group',
                'name'     => 'Others',
                'value'    => 'others',
            ],

        ]);

    }
}
