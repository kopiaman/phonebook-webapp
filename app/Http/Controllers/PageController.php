<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Lookup;
use Illuminate\Http\Request;
use Validator;

class PageController extends Controller
{
    public function listing()
    {
        $contacts = Contact::orderBy('last_name')->paginate(5);

        return view('blocks.list', compact('contacts'));
    }

    public function create()
    {
        $types = Lookup::where('category', 'contact_type')->get();

        $groups = Lookup::where('category', 'contact_group')->get();

        return view('blocks.create', compact('types', 'groups'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone_no'   => 'required',
            'type'       => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();

        }

        $insert = Contact::create([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'phone_no'   => $request->phone_no,
            'type'       => $request->type,
            'group'      => $request->group,

        ]);

        if ($insert) {
            return redirect('/');
        }
    }
}
