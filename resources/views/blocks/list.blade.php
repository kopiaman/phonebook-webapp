@extends('layouts.master')
@section('content')
<ul class="collection">

	@foreach ( $contacts AS $contact)
	<li class="collection-item avatar">

				@if($contact->type == 'home')
					<i class="material-icons circle teal">home</i>
				@elseif($contact->type == 'work')
					<i class="material-icons circle cyan">work</i>
				@elseif($contact->type =='cellular')
					 <i class="material-icons circle blue">phone_iphone</i>
				@else
					<i class="material-icons circle purple">call</i>
				@endif



		<span class="title">
			{{ $contact->last_name}} {{$contact->first_name}}
		</span>
		<p>
			<span class='grey-text'>{{ $contact->group}}</span> <br/>
			<span class=''>{{ $contact->phone_no}}</span>
		</p>

		<a href="#!" class="secondary-content">

			<i class="material-icons">
				call
			</i>
		</a>

	</li>
	@endforeach
	<li class="collection-item avatar" style='padding-left:0'>
	<span class='center-align'>
	{{ $contacts->links() }}
	</span>
	</li>

</ul>
@stop
