
@extends('layouts.master')


@section('content')

	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif



	<br/>
	<h3>CREATE NEW CONTACT</h3>

	<div class='box_container'>
	    <form action='/store' method='POST'>
	    	{{ csrf_field() }}

	      <div class="row">

	        <div class="input-field col s6">
	          <input id="first_name" name='first_name' type="text" class="validate" value="{{ old('first_name') }}" required>
	          <label for="first_name">First Name</label>
	        </div>

	        <div class="input-field col s6">
	          <input id="last_name" name='last_name' type="text" class="validate" value="{{ old('last_name') }}" required>
	          <label for="last_name">Last Name</label>
	        </div>

	      </div>

	      <div class="row">

	        <div class="input-field col s6">
	          <input id="phone_no" name='phone_no' type="number" class="validate"  value="{{ old('phone_no') }}" required>
	          <label for="phone_no">Phone Number</label>
	        </div>

	        <div class="input-field col s6">
			    <select name='type' required>
			      <option value="" disabled selected>Choose your option</option>
			       @foreach( $types AS $type)
			      <option value="{{$type->value}}">{{$type->name}}</option>
			      @endforeach
			    </select>
			    <label>Type</label>
			  </div>

	      </div>

	      <div class="row">

	        <div class="input-field col s6">
			    <select name='group'>
			      <option value="" disabled selected>Choose your option</option>
			      @foreach( $groups AS $group)
			      <option value="{{$group->value}}">{{$group->name}}</option>
			      @endforeach

			    </select>
			    <label>Group</label>
			  </div>

			   <div class="input-field col s6">
	          <input id="notes" name='notes' type="text" class="validate" value="{{ old('notes') }}">
	          <label for="notes">Notes</label>
	        </div>

	      </div>

	      <button type='submit' class='btn orange'>SUBMIT</button>

	    </form>
	</div>

@stop

@section('scripts')
<script>
$(document).ready(function(){
    $('select').formSelect();
  });
</script>
@stop
