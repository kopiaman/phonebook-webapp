<nav class="nav-extended pink accent-3 ">
    <div class="nav-wrapper ">
      <a href="/" class="brand-logo">PhoneBook</a>

    </div>
    <div class="nav-content">

      <a class="btn-floating btn-large halfway-fab waves-effect waves-light amber darken-4" href='/create'>
        <i class="material-icons">add</i>
      </a>
    </div>
  </nav>
