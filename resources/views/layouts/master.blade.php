 <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">

      <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">

      <link href="https://fonts.googleapis.com/css?family=Ruda:400,700" rel="stylesheet">

      <link rel="stylesheet"  href='/css/style.css'>
      @yield('head')

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    </head>

    <body class='purple accent-4'>

      @include('partials.header')

      <section id='main'>
        <div class=" container">
         @yield('content')
        </div>
      </section>


      @include('partials.footer')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

    @yield('scripts')
    </body>

  </html>
